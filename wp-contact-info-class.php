<?php

class ChContactInfoWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 
			'classname' => 'ch-contact-info-widget',
		);
		parent::__construct( 'ch-contact-info-widget', 'Contact Info Widget', $widget_ops );
        }
        function form( $instance ) {
                $ch_site_location   = isset($instance['ch-site-location-name']) ? $instance['ch-site-location-name'] : null;
                $ch_site_open_days  = isset($instance['ch-site-open-days']) ? $instance['ch-site-open-days'] : null;
                $ch_site_phone      = isset($instance['ch-site-phone']) ? $instance['ch-site-phone'] : null;
                $ch_site_mail       = isset($instance['ch-site-mail']) ? $instance['ch-site-mail'] : null;
                $ch_site_skype      = isset($instance['ch-site-skype']) ? $instance['ch-site-skype'] : null;
              ?> 
        <label for="<?php echo esc_attr($this->get_field_id( 'ch-site-address') ); ?>"><?php esc_html_e( 'Location :','noterepair' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-site-address' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-site-location-name' )); ?>" type="text" value = '<?php echo esc_attr($ch_site_location); ?>'/>

        <label for="<?php echo esc_attr($this->get_field_id( 'ch-site-open-dys') ); ?>"><?php esc_html_e( 'Open Days :','noterepair' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-site-open-dys' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-site-open-days' )); ?>" type="text" value = '<?php echo esc_attr($ch_site_open_days); ?>'/>

        <label for="<?php echo esc_attr($this->get_field_id( 'ch-site-phn') ); ?>"><?php esc_html_e( 'Phone :','noterepair' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-site-phn' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-site-phone' )); ?>" type="text" value = '<?php echo esc_attr($ch_site_phone); ?>'/>

        <label for="<?php echo esc_attr($this->get_field_id( 'ch-site-mail') ); ?>"><?php esc_html_e( 'Mail :','noterepair' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-site-mail' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-site-mail' )); ?>" type="text" value = '<?php echo esc_attr($ch_site_mail); ?>'/>

        <label for="<?php echo esc_attr($this->get_field_id( 'ch-site-skype') ); ?>"><?php esc_html_e( 'Skype :','noterepair' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-site-skype' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-site-skype' )); ?>" type="text" value = '<?php echo esc_attr($ch_site_skype); ?>'/>
             <?php
         }
	function widget( $args, $instance ) {
		 ?>
        <div class = 'ch-one-widget ch-contact-info-widget'>
                        <div class = 'ch-widget-container'>
                                <span class = 'ch-widget-title'>Contact Info</span>
                                <?php if(strlen($instance['ch-site-location-name'])>0) : ?>
                                <span class = 'ch-widget-location info-widget-item'>
                                        <span class = 'ch-icon-container'>
                                           <i class = 'fa fa-map-marker'></i>
                                           </span><?php echo esc_html($instance['ch-site-location-name']); ?>
                                </span>
                                 <?php endif ; ?>
                                 <?php if(strlen($instance['ch-site-open-days'])> 0): ?>
                                <span class = 'ch-widget-location info-widget-item'>
                                        <span class = 'ch-icon-container'>
                                            <i class = 'fa fa-clock-o'></i>
                                        </span><?php echo esc_html($instance['ch-site-open-days']); ?>
                                </span>
                                <?php endif ; ?>
                                <?php if(strlen($instance['ch-site-phone'])) : ?>
                                <span class = 'ch-widget-phone info-widget-item'>
                                        <span class = 'ch-icon-container'>
                                               <i class = 'fa fa-phone'></i>
                                        </span><?php echo esc_html($instance['ch-site-phone']) ; ?>
                                </span>
                                <?php endif ; ?>
                                <?php if(isset($instance['ch-site-mail'])): ?>
                                <span class = 'ch-widget-mail info-widget-item'>
                                        <span class = 'ch-icon-container'>
                                                <i class = 'fa fa-paper-plane-o'></i>
                                        </span><?php echo esc_html($instance['ch-site-mail']) ; ?>
                                </span>
                                <?php endif ; ?>
                                <?php if(isset($instance['ch-site-skype'])): ?>
                                <span class = 'ch-widget-skype info-widget-item'>
                                    <span class = 'ch-icon-container'>
                                            <i class = 'fa fa-skype'></i>
                                    </span><?php echo esc_html($instance['ch-site-skype']) ; ?>
                                 </span>
                                 <?php endif ; ?>
                        </div>
                 </div><!-- end widget -->
		 <?php
	}
        function update( $new_instance, $old_instance ) {
         $old_instance['ch-site-location-name'] = $new_instance['ch-site-location-name'];
         $old_instance['ch-site-open-days']     = $new_instance['ch-site-open-days'];
         $old_instance['ch-site-phone']         = $new_instance['ch-site-phone'];
         $old_instance['ch-site-mail']          = $new_instance['ch-site-mail'];
         $old_instance['ch-site-skype']         = $new_instance['ch-site-skype'];
         return $old_instance; 
         }
}
function ch_register_widgets() {
	register_widget( 'ChContactInfoWidget' );
}
add_action( 'widgets_init', 'ch_register_widgets' );

?>